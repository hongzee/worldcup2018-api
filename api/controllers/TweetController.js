const Tweet = require('../models/tweet');
const Sequelize = require('sequelize');

const TweetController = () => {
  const add = (req, res) => {
    const { body } = req;

    if (body.tweet) {
      return Tweet.create({
        tweet: body.tweet,
        added_by: body.added_by || 'SYSTEM',
      })
        .then(tweet => res.status(200).json({ tweet }))
        .catch(err => {
          console.log(err);
          return res.status(500).json({ msg: 'Internal server error' });
        });
    }

    return res.status(400).json({ msg: 'No tweet content was passed' });
  };

  const remove = (req, res) => {
    const { body } = req;

    if (body.id) {
      return Tweet.destroy({
        where: { id: body.id },
      })
        .then(() => res.status(200).json({ id: body.id }))
        .catch(err => {
          console.log(err);
          return res.status(500).json({ msg: 'Internal server error' });
        });
    }

    return res.status(400).json({ msg: 'No tweet id was passed' });
  };

  const getRand = (req, res) => {
    Tweet.findOne({ order: Sequelize.literal('rand()') })
      // eslint-disable-next-line
      .then(
        // eslint-disable-next-line
        tweet =>
          tweet
            ? res.status(200).json({ tweet })
            : // eslint-disable-next-line
              res.status(400).json({ msg: 'No tweets in database' })
        // eslint-disable-next-line
      )
      .catch(err => {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      });
  };

  const getAll = (req, res) => {
    Tweet.findAll()
      .then(tweets => res.status(200).json({ tweets }))
      .catch(err => {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      });
  };

  return {
    add,
    remove,
    getRand,
    getAll,
  };
};

module.exports = TweetController;

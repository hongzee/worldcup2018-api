const Email = require('../models/email');
const Sequelize = require('sequelize');

const EmailController = () => {
  const add = (req, res) => {
    const { body } = req;

    if (body.email) {
      return Email.create({
        email: body.email,
        added_by: body.added_by || 'SYSTEM',
      })
        .then(email => res.status(200).json({ email }))
        .catch(err => {
          console.log(err);
          return res.status(500).json({ msg: 'Internal server error' });
        });
    }

    return res.status(400).json({ msg: 'No email content was passed' });
  };

  const remove = (req, res) => {
    const { body } = req;

    if (body.id) {
      return Email.destroy({
        where: { id: body.id },
      })
        .then(() => res.status(200).json({ id: body.id }))
        .catch(err => {
          console.log(err);
          return res.status(500).json({ msg: 'Internal server error' });
        });
    }

    return res.status(400).json({ msg: 'No email id was passed' });
  };

  const getRand = (req, res) => {
    Email.findOne({ order: Sequelize.literal('rand()') })
      // eslint-disable-next-line
      .then(
        // eslint-disable-next-line
        email =>
          email
            ? res.status(200).json({ email })
            : // eslint-disable-next-line
              res.status(400).json({ msg: 'No emails in database' })
        // eslint-disable-next-line
      )
      .catch(err => {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      });
  };

  const getAll = (req, res) => {
    Email.findAll()
      .then(emails => res.status(200).json({ emails }))
      .catch(err => {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      });
  };

  return {
    add,
    remove,
    getRand,
    getAll,
  };
};

module.exports = EmailController;

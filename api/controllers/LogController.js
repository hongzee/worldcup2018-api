const Log = require('../models/log');

const LogController = () => {
  const add = (req, res) => {
    const { body } = req;

    if (
      body.email &&
      body.subject &&
      body.firstname &&
      body.lastname &&
      body.useremail
    ) {
      return Log.create({
        email: body.email,
        subject: body.subject,
        firstname: body.firstname,
        lastname: body.lastname,
        useremail: body.useremail,
        userpostal: body.userpostal || '',
      })
        .then(log => res.status(200).json({ log }))
        .catch(err => {
          console.log(err);
          return res.status(500).json({ msg: 'Internal server error' });
        });
    }

    return res.status(400).json({ msg: 'Log content was not passed' });
  };

  const remove = (req, res) => {
    const { body } = req;

    if (body.id) {
      return Log.destroy({
        where: { id: body.id },
      })
        .then(() => res.status(200).json({ id: body.id }))
        .catch(err => {
          console.log(err);
          return res.status(500).json({ msg: 'Internal server error' });
        });
    }

    return res.status(400).json({ msg: 'No log id was passed' });
  };

  const getAll = (req, res) => {
    Log.findAll()
      .then(logs => res.status(200).json({ logs }))
      .catch(err => {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      });
  };

  return {
    add,
    remove,
    getAll,
  };
};

module.exports = LogController;

const Subject = require('../models/subject');
const Sequelize = require('sequelize');

const SubjectController = () => {
  const add = (req, res) => {
    const { body } = req;

    if (body.subject) {
      return Subject.create({
        subject: body.subject,
        added_by: body.added_by || 'SYSTEM',
      })
        .then(subject => res.status(200).json({ subject }))
        .catch(err => {
          console.log(err);
          return res.status(500).json({ msg: 'Internal server error' });
        });
    }

    return res.status(400).json({ msg: 'No subject content was passed' });
  };

  const remove = (req, res) => {
    const { body } = req;

    if (body.id) {
      return Subject.destroy({
        where: { id: body.id },
      })
        .then(() => res.status(200).json({ id: body.id }))
        .catch(err => {
          console.log(err);
          return res.status(500).json({ msg: 'Internal server error' });
        });
    }

    return res.status(400).json({ msg: 'No subject id was passed' });
  };

  const getRand = (req, res) => {
    Subject.findOne({ order: Sequelize.literal('rand()') })
      // eslint-disable-next-line
      .then(
        // eslint-disable-next-line
        subject =>
          subject
            ? res.status(200).json({ subject })
            : // eslint-disable-next-line
              res.status(400).json({ msg: 'No subjects in database' })
        // eslint-disable-next-line
      )
      .catch(err => {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      });
  };

  const getAll = (req, res) => {
    Subject.findAll()
      .then(subjects => res.status(200).json({ subjects }))
      .catch(err => {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      });
  };

  return {
    add,
    remove,
    getRand,
    getAll,
  };
};

module.exports = SubjectController;

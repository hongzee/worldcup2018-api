const Sequelize = require('sequelize');

const sequelize = require('../../config/database');

const tableName = 'emails';

const Email = sequelize.define('Email', {
  id: {
    type: Sequelize.INTEGER(11).UNSIGNED,
    primaryKey: true,
    autoIncrement: true,
  },
  email: {
    type: Sequelize.STRING,
  },
  added_by: {
    type: Sequelize.STRING,
  },
}, { tableName });

module.exports = Email;

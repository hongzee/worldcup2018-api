const Sequelize = require('sequelize');

const sequelize = require('../../config/database');

const tableName = 'logs';

const Log = sequelize.define('Log', {
  id: {
    type: Sequelize.INTEGER(11).UNSIGNED,
    primaryKey: true,
    autoIncrement: true,
  },
  email: {
    type: Sequelize.INTEGER,
  },
  subject: {
    type: Sequelize.INTEGER,
  },
  firstname: {
    type: Sequelize.STRING,
  },
  lastname: {
    type: Sequelize.STRING,
  },
  useremail: {
    type: Sequelize.STRING,
  },
  userpostal: {
    type: Sequelize.STRING,
  },
}, { tableName });

module.exports = Log;

const Sequelize = require('sequelize');

const sequelize = require('../../config/database');

const tableName = 'tweets';

const Tweet = sequelize.define('Tweet', {
  id: {
    type: Sequelize.INTEGER(11).UNSIGNED,
    primaryKey: true,
    autoIncrement: true,
  },
  tweet: {
    type: Sequelize.STRING,
  },
  added_by: {
    type: Sequelize.STRING,
  },
}, { tableName });

module.exports = Tweet;

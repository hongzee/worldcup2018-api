const Sequelize = require('sequelize');

const sequelize = require('../../config/database');

const tableName = 'subjects';

const Subject = sequelize.define('Subject', {
  id: {
    type: Sequelize.INTEGER(11).UNSIGNED,
    primaryKey: true,
    autoIncrement: true,
  },
  subject: {
    type: Sequelize.STRING,
  },
  added_by: {
    type: Sequelize.STRING,
  },
}, { tableName });

module.exports = Subject;

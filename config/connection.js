const development = {
  database: 'dev',
  host: 'randomization.ctgd9vkuiplg.us-east-2.rds.amazonaws.com',
  username: 'takepoints',
  password: 'KJt-M4S-b8W-WUe',
  dialect: 'mysql',
};

const testing = {
  database: 'dev',
  host: 'randomization.ctgd9vkuiplg.us-east-2.rds.amazonaws.com',
  username: 'takepoints',
  password: 'KJt-M4S-b8W-WUe',
  dialect: 'mysql',
};

const production = {
  database: process.env.DB_NAME,
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  host: process.env.DB_HOST,
  dialect: 'mysql',
};

module.exports = {
  development,
  testing,
  production,
};

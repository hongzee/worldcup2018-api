const privateRoutes = require('./routes/privateRoutes');
const publicRoutes = require('./routes/publicRoutes');

module.exports = {
  keep: false,
  migrate: true,
  privateRoutes,
  publicRoutes,
  port: process.env.PORT || '5000',
};

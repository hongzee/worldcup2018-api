module.exports = {
  'GET /users': 'UserController.getAll',
  // Email
  'POST /add-email': 'EmailController.add',
  'DELETE /remove-email': 'EmailController.remove',
  'GET /emails': 'EmailController.getAll',
  // Subject
  'POST /add-subject': 'SubjectController.add',
  'DELETE /remove-subject': 'SubjectController.remove',
  'GET /subjects': 'SubjectController.getAll',
  // Tweets
  'POST /add-tweet': 'TweetController.add',
  'DELETE /remove-tweet': 'TweetController.remove',
  'GET /tweets': 'TweetController.getAll',
  // Logs
  'GET /logs': 'LogController.getAll',
  'DELETE /remove-log': 'LogController.remove',
};

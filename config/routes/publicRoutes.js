module.exports = {
  'POST /user': 'UserController.register',
  'POST /register': 'UserController.register', // alias for POST /user
  'POST /login': 'UserController.login',
  'POST /validate': 'UserController.validate',
  // Email
  'GET /email': 'EmailController.getRand',
  // Subject
  'GET /subject': 'SubjectController.getRand',
  // Tweets
  'GET /tweet': 'TweetController.getRand',
  // Logs
  'POST /log': 'LogController.add',
};
